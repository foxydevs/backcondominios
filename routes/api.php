<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::resource('accesos', 'AccesosController');
Route::resource('alertas', 'AlertasController');
Route::resource('categorias', 'CategoriasController');
Route::resource('citas', 'CitasController');
Route::resource('cobros', 'CobrosController');
Route::resource('cotizaciones', 'CotizacionesController');
Route::resource('entidades', 'EntidadesController');
Route::resource('mensajes', 'EntidadesController');
Route::resource('modulos', 'ModulosController');
Route::resource('objetos_perdidos', 'ObjetosPerdidosController');
Route::resource('proyectos', 'ProyectosController');
Route::resource('recomendaciones', 'RecomendacionesController');
Route::resource('reuniones', 'ReunionesController');
Route::resource('roles', 'RolesController');
Route::resource('servicios', 'ServiciosController');
Route::resource('sucursales', 'SucursalesController');
Route::resource('usuarios', 'UsuariosController');
Route::resource('visitas', 'VisitasController');
Route::resource('votaciones', 'VotacionesController');

Route::post('login', 'AuthenticateController@login');

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});
