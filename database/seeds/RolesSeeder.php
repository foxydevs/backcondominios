<?php

use Illuminate\Database\Seeder;

class RolesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('roles')->insert([
            'descripcion'       => 'Administrador',   
            'modulos'           => '123456',
            'estado'            => 1,
            'deleted_at'        => null,
            'created_at'        => date('Y-m-d H:m:s'),
            'updated_at'        => date('Y-m-d H:m:s')
        ]);

        DB::table('roles')->insert([
            'descripcion'       => 'Guardia',   
            'modulos'           => '123456',
            'estado'            => 1,
            'deleted_at'        => null,
            'created_at'        => date('Y-m-d H:m:s'),
            'updated_at'        => date('Y-m-d H:m:s')
        ]);
        
        DB::table('roles')->insert([
            'descripcion'       => 'Vecino',   
            'modulos'           => '123456',
            'estado'            => 1,
            'deleted_at'        => null,
            'created_at'        => date('Y-m-d H:m:s'),
            'updated_at'        => date('Y-m-d H:m:s')
        ]);
    }
}
