<?php

use Illuminate\Database\Seeder;

class UsuariosSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('usuarios')->insert([
            'username'           => 'admin',   
            'password'           => bcrypt('foxylabs'),
            'email'              => 'admin@foxylabs.gt',
            'privileges'         => 1,
            'estado'             => 1,
            'rol'                => 1,
            'deleted_at'         => null,
            'created_at'         => date('Y-m-d H:m:s'),
            'updated_at'         => date('Y-m-d H:m:s')
        ]);

        DB::table('usuarios')->insert([
            'username'           => 'guardia',   
            'password'           => bcrypt('foxylabs'),
            'email'              => 'guardia@foxylabs.gt',
            'privileges'         => 1,
            'estado'             => 1,
            'rol'                => 2,
            'deleted_at'         => null,
            'created_at'         => date('Y-m-d H:m:s'),
            'updated_at'         => date('Y-m-d H:m:s')
        ]);

        DB::table('usuarios')->insert([
            'username'           => 'vecino',   
            'password'           => bcrypt('foxylabs'),
            'email'              => 'vecino@foxylabs.gt',
            'privileges'         => 1,
            'estado'             => 1,
            'rol'                => 3,
            'deleted_at'         => null,
            'created_at'         => date('Y-m-d H:m:s'),
            'updated_at'         => date('Y-m-d H:m:s')
        ]);
    }
}
