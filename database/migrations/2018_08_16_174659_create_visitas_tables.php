<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateVisitasTables extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('visitas', function (Blueprint $table)
        {
            $table->increments('id');
            $table->timestamp('fecha_hora')->useCurrent()->default(null);
            $table->tinyInteger('estadoVisita')->nullable()->default(null);
            $table->tinyInteger('estado')->nullable()->default(1);

            $table->integer('usuario')->unsigned()->default(null);
            $table->foreign('usuario')->references('id')->on('usuarios')->onDelete('cascade');
            $table->integer('entidad')->unsigned()->default(null);
            $table->foreign('entidad')->references('id')->on('entidades')->onDelete('cascade');

            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('visitas');
    }
}
