<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateObjetosPerdidosTables extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('objetos_perdidos', function (Blueprint $table)
        {
            $table->increments('id');
            $table->string('nombreEncontrado')->nullable()->default(null);
            $table->string('descripcionEncontrado')->nullable()->default(null);
            $table->string('nombrePerdido')->nullable()->default(null);
            $table->string('descripcionPerdido')->nullable()->default(null);
            $table->tinyInteger('estado')->nullable()->default(1);

            $table->integer('usuarioDuenio')->unsigned()->nullable()->default(null);
            $table->foreign('usuarioDuenio')->references('id')->on('usuarios')->onDelete('cascade');
            $table->integer('usuarioReporte')->unsigned()->nullabel()->default(null);
            $table->foreign('usuarioReporte')->references('id')->on('usuarios')->onDelete('cascade');

            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('objetos_perdidos');
    }
}
