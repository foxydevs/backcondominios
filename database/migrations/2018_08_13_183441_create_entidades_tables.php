<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEntidadesTables extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('entidades', function (Blueprint $table){
            $table->increments('id');
            $table->string('metros')->nullable()->default(null);
            $table->string('dormitorios')->nullable()->default(null);
            $table->string('baños')->nullable()->default(null);
            $table->string('salas')->nullable()->default(null);
            $table->string('comedores')->nullable()->default(null);
            $table->string('precio')->nullable()->default(null);
            $table->string('personas')->nullable()->default(null);
            $table->string('direccion')->nullable()->default(null);
            $table->string('descripcion')->nullable()->default(null);
            $table->tinyInteger('estado')->nullable()->default(1);

            $table->integer('condominio')->unsigned()->nullable()->default(null);
            $table->foreign('condominio')->references('id')->on('sucursales')->onDelete('cascade');
           
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('entidades');
    }
}
