<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateVotacionesTables extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('votaciones', function (Blueprint $table)
        {
            $table->increments('id');
            $table->tinyInteger('estado')->nullable()->default(1);

            $table->integer('cotizacion')->unsigned()->default(null);
            $table->foreign('cotizacion')->references('id')->on('cotizaciones')->onDelete('cascade');
            $table->integer('usuario')->unsigned()->default(null);
            $table->foreign('usuario')->references('id')->on('usuarios')->onDelete('cascade');

            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('votaciones');
    }
}
