<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMensajesTables extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('mensajes', function(Blueprint $table)
        {
            $table->increments('id');
            $table->string('tema')->nullable()->default(null);
            $table->string('mensaje')->nullable()->default(null);
            $table->tinyInteger('estado')->nullable()->default(1);

            $table->integer('usuarioEmisor')->unsigned()->nullable()->default(null);
            $table->foreign('usuarioEmisor')->references('id')->on('usuarios')->onDelete('cascade');

            $table->integer('usuarioReceptor')->unsigned()->nullable()->default(null);
            $table->foreign('usuarioReceptor')->references('id')->on('usuarios')->onDelete('cascade');

            $table->integer('usuarioAdministrador')->unsigned()->nullable()->default(null);
            $table->foreign('usuarioAdministrador')->references('id')->on('usuarios')->onDelete('cascade');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('mensajes');
    }
}
