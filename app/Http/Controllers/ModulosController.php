<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Modulos;
use Response;
use Validator;

class ModulosController extends Controller
{
    public function index()
    {
        return Response::json(Modulos::all(), 200);
    }

    public function create()
    {

    }

    public function store(Request $request)
    {
        $validator = validator::make($request->all(), [
            'nombre'            => 'required',
            'icono'             => 'required',
            'link'              => 'required'
        ]);
        if($validator -> fails())
        {
            $returnData = array (
                'status'    => 400,
                'message'   => 'Invalid Parameters',
                'validator' => $validator
            );

            return Response::json($returnData, 400);
        }
        else{
            try
            {
                $newObject = new Modulos();
                $newObject->nombre          = $request->get('nombre');
                $newObject->icono           = $request->get('icono');
                $newObject->link            = $request->get('link');
                $newObject->dir             = $request->get('dir');
                $newObject->refId           = $request->get('refId');
                $newObject->tipo            = $request->get('tipo', 0);
                $newObject->orden           = $request->get('orden', 0);
                $newObject->save();
                return Response::json($newObject, 200);
            }
            catch(Exception $e)
            {
                $returnData = array(
                    'status'    => 500,
                    'message'   => $e->getMessage()
                );
                return Response::json($returnData, 500);
            }
        }
    }

    public function show($id)
    {
        $objectSee = Modulos::find($id);
        if($objectSee)
        {
            return Response::json($objectSee, 200);
        }
        else
        {
            $returnData = array (
                'status'    => 404,
                'message'   => 'No record found'
            );
            return Response::json($returnData, 404);
        }
    }

    public function edit($id)
    {
        //
    }

    public function update(Request $request, $id)
    {
        $objectUpdate = Modulos::find($id);
        if ($objectUpdate)
        {
            try
            {
                $objectUpdate->nombre           = $request->get('nombre', $objectUpdate->nombre);
                $objectUpdate->icono            = $request->get('icono', $objectUpdate->icono);
                $objectUpdate->link             = $request->get('link', $objectUpdate->link);
                $objectUpdate->dir              = $request->get('dir', $objectUpdate->dir);
                $objectUpdate->refId            = $request->get('refId', $objectUpdate->refId);
                $objectUpdate->orden            = $request->get('orden', $objectUpdate->orden);
                $objectUpdate->tipo             = $request->get('tipo', $objectUpdate->tipo);
                $objectupdate->save();
                return Response::json($objectUpdate, 200);
            }
            catch(Exception $e)
            {
                $returnData = array (
                    'status'    => 500,
                    'message'   => $e->getMessage()
                );
                return Response::json($returnData, 500);
            }
        }
        else
        {
            $returnData = array(
                'status'        => 404,
                'message'       => 'No record found'
            );
            return Response::json($returnData, 404);
        }
    }

    public function destroy($id)
    {
        $objectDelete = Modulos::find($id);
        if($objectDelete)
        {
            try
            {
                Modulos::destroy($id);
                return Response::json($objectDelete, 200);
            }
            catch(Exception $e)
            {
                $returnData = array(
                    'status'    => 500,
                    'message'   => $e->getmessage()
                );
                return Response::json($returnData, 500);
            }
        }
        else
        {
            $returnData = array(
                'status'    => 404,
                'message'   => 'No record found'
            );
            return Response::json($returnData, 404);
        }
    }
}
