<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\ObjetosPerdidos;
use Response;
use Validator;

class ObjetosPerdidosController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return Response::json(ObjetosPerdidos::all(), 200);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'nombreEncontrado'          => 'required',
            'descripcionEncontrado'     => 'required',
            'nombrePerdido'             => 'required',
            'descripcionPerdido'        => 'required',
        ]);
        if ( $validator->fails() ) {
            $returnData = array (
                'status' => 400,
                'message' => 'Invalid Parameters',
                'validator' => $validator
            );
            return Response::json($returnData, 400);
        }
        else {
            try
            {
                $newObject = new ObjetosPerdidos();
                $newObject->nombreEncontrado          = $request->get('nombreEncontrado');
                $newObject->descripcionEncontrado     = $request->get('descripcionEncontrado');
                $newObject->nombrePerdido             = $request->get('nombrePerdido');
                $newObject->descripcionPerdido        = $request->get('descripcionPerdido');  
                $newObject->usuarioDuenio             = $request->get('usuarioDuenio');
                $newObject->usuarioReporte            = $request->get('usuarioReporte');
                $newObject->save();
                return Response($newObject, 500);
            }
            catch(Exception $e)
            {
                $returnData = array(
                    'status'    => 500,
                    'message'   => $e->getMessage()
                );
                return Response::json($returnData, 500);
            }
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $objectSee = ObjetosPerdidos::find($id);
        if($objectSee)
        {
            return Response::json($objectSee, 200);
        }
        else
        {
            $returnData = array(
                'status'    => 404,
                'message'   => 'No record found'
            );
            return Response::json($returnData, 404);
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $objectUpdate = ObjetosPerdidos::find($id);
        if($objectUpdate)
        {
            try
            {
                $objectUpdate->nombreEncontrado           = $request->get('nombreEncontrado', $objectUpdate->nombreEncontrado);
                $objectUpdate->descripcionEncontrado      = $request->get('descripcionEncontrado', $objectUpdate->descripcionEncontrado);
                $objectUpdate->nombrePerdido              = $request->get('nombrePerdido', $objectUpdate->nombrePerdido);
                $objectUpdate->descripcionPerdido         = $request->get('descripcionPerdido', $objectUpdate->descripcionPerdido);
                $objectUpdate->save();
                return Response::json($objectUpdate, 200);
            }
            catch(Exception $e)
            {
                $returnData = array(
                    'status'    => 500,
                    'message'   => $e->getMessage()
                );
                return Response::json($returnData, 500);
            }
        }
        else
        {
            $returnData = array(
                'status'    => 404,
                'message'   => 'No record found'
            );
            return Response::json($returnData, 404);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $objectDelete = ObjetosPerdidos::find($id);
        if ($objectDelete) {
            try {
                ObjetosPerdidos::destroy($id);
                return Response::json($objectDelete, 200);
            } catch (Exception $e) {
                $returnData = array (
                    'status' => 500,
                    'message' => $e->getMessage()
                );
                return Response::json($returnData, 500);
            }
        }
        else {
            $returnData = array (
                'status' => 404,
                'message' => 'No record found'
            );
            return Response::json($returnData, 404);
        }
    }
}
