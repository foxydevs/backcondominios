<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Entidades;
use Response;
use Validator;

class EntidadesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return Response::json(Entidades::all(), 200);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = validator::make($request->all(), [
            'metros'                => 'required',
            'dormitorios'           => 'required',
            'banios'                => 'required',
            'salas'                 => 'required',
            'comedores'             => 'required',
            'precio'                => 'required',
            'personas'              => 'required',
            'direccion'             => 'required',
            'descripcion'           => 'required',  
        ]);
        if($validator -> fails())
        {
            $returnData = array(
                'status'    => 400,
                'message'   => 'Invalid Parameters',
                'validator' => $validator
            );
            return Response::json($returnData, 400);
        }
        else
        {
            try
            {
                $newObject = new Entidades();
                $newObject->metros          = $request->get('metros');
                $newObject->dormitorios     = $request->get('dormitorios');
                $newObject->banios          = $request->get('banios');
                $newObject->salas           = $request->get('salas');
                $newObject->comedores       = $request->get('comedores');
                $newObject->precio          = $request->get('precio');
                $newObject->personas        = $request->get('personas');
                $newObject->direccion       = $request->get('direccion');
                $newObject->descripcion     = $request->get('descripcion');
                $newObject->save();
                return Response::json($newObject, 200);
            }
            catch(Exception $e)
            {
                $returnData = array(
                    'status'    => 500,
                    'message'   => $e->getMessage()
                );
                return Response::json($returnData, 500);
            }
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $objectSee = Entidades::find($id);
        if ($objectSee) 
        {
            return Response::json($objectSee, 200);
        }
        else 
        {
            $returnData = array (
                'status' => 404,
                'message' => 'No record found'
            );
            return Response::json($returnData, 404);
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $objectUpdate = Entidades::find($id);
        if ($objectUpdate) {
            try {
                $objectUpdate->metros               = $request->get('metros', $objectUpdate->metros);
                $objectUpdate->dormitorios          = $request->get('dormitorios', $objectUpdate->dormitorios);
                $objectUpdate->banios               = $request->get('banios', $objectUpdate->banios);
                $objectUpdate->salas                = $request->get('salas', $objectUpdate->salas);
                $objectUpdate->comedores            = $request->get('comedores', $objectUpdate->comedores);
                $objectUpdate->precio               = $request->get('precio', $objectUpdate->precio);
                $objectUpdate->personas             = $request->get('personas', $objectUpdate->personas);
                $objectUpdate->direccion            = $request->get('direccion', $objectUpdate->direccion);
                $objectUpdate->descripcion          = $request->get('descripcion', $objectUpdate->descripcion);
                $objectUpdate->save();
                return Response::json($objectUpdate, 200);
            } 
            catch (Exception $e) {
                $returnData = array (
                    'status' => 500,
                    'message' => $e->getMessage()
                );
                return Response::json($returnData, 500);
            }
        }
        else {
            $returnData = array (
                'status' => 404,
                'message' => 'No record found'
            );
            return Response::json($returnData, 404);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $objectDelete = Entidades::find($id);
        if ($objectDelete) {
            try {
                Entidades::destroy($id);
                return Response::json($objectDelete, 200);
            } catch (Exception $e) {
                $returnData = array (
                    'status' => 500,
                    'message' => $e->getMessage()
                );
                return Response::json($returnData, 500);
            }
        }
        else {
            $returnData = array (
                'status' => 404,
                'message' => 'No record found'
            );
            return Response::json($returnData, 404);
        }
    }
}
