<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Roles;
use Response;
use Validator;

class RolesController extends Controller
{
    public function index()
    {
        return Response::json(Roles::all(), 200);
    }

    public function create()
    {

    }

    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'descripcion'       =>  'required'
        ]);
        if($validator->fails())
        {
            $returnData = array(
                'status'    => 404,
                'message'   => 'Invalid Parameters',
                'validator' => $validator
            );
            return Response::json($returnData, 404);
        }
        else
        {
            try
            {
                $newObject = new Roles();
                $newObject->descripcion         = $request->get('descripcion');
                $newObject->save();
                return Response::json($newObject, 200);
            }
            catch(Exception $e)
            {
                $returnData = array(
                    'status'    => 500,
                    'message'   =>$e->getMessage()
                );
                return Response::json($returnData, 500);
            }
        }
    }

    public function show($id)
    {
        $objectSee = Roles::find($id);
        if($objectSee)
        {
            return Response::json($objectSee, 200);
        }
        else
        {
            $returnData = array(
                'status'    => 404,
                'message'   => 'No record found'
            );
            return Response::json($returnData, 404);
        }
    }

    public function edit($id)
    {

    }

    public function update(Request $request, $id)
    {
        $objectUpdate = Roles::find($id);
        if($objectUpdate)
        {
            try
            {
                $objectUpdate->descripcion = $request->get('descripcion', $objectUpdate->descripcion);
                $objectUpdate->save();
                return Response::json($objectUpdate, 200);
            }
            catch(Exception $e)
            {
                $returnData = array(
                    'status'    => 500,
                    'message'   => $e->getMessage()
                );
                return Response::json($returnData, 500);
            }
        }
        else
        {
            $returnData = array(
                'status'    => 404,
                'message'   => 'No record found'
            );
            return Resposne::json($returnData, 404);
        }
    }

    public function destroy($id)
    {
        $objectDelete = Roles::find($id);
        if($objectDelete)
        {
            try
            {
                Roles::destroy($id);
                return Response::json($objectDelete, 200);
            }
            catch(Exception $e)
            {
                $returnData = array(
                    'status'    => 500,
                    'message'   => $e->getMessage()
                );
                return Response::json($returnData, 500);
            }
        }
        else
        {
            $returnData = array(
                'status'    => 404,
                'message'   => 'No record found'
            );
            return Response::json($returnData, 404);
        }
    }
}
