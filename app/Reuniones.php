<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Reuniones extends Model
{
    use SoftDeletes;
    protected $table = 'reuniones';

    public function reuniones()
    {
        return $this->hasOne('App\Sucursales', 'id', 'condominio');
    }
}
