<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Cobros extends Model
{
    use SoftDeletes;
    protected $table = 'cobros';

    public function sucursales()
    {
        return $this->hasOne('App\Sucursales', 'id', 'condominio');
    }
}
