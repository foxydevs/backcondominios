<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ObjetosPerdidos extends Model
{
    use SoftDeletes;
    protected $table = 'objetos_perdidos';

    public function usuarioDuenio()
    {
        return $this->hasOne('App\Usuarios', 'id', 'usuarioDuenio');
    }

    public function usuarioReporte()
    {
        return $this->hasOne('App\Usuarios', 'id', 'usuarioReporte');
    }
}
