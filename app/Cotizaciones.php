<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Cotizaciones extends Model
{
    use SoftDeletes;
    protected $table = 'cotizaciones';

    public function proyectos()
    {
        return $this->hasOne('App\Proyectos', 'id', 'proyecto');
    }
}
