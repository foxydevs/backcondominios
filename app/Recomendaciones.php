<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Recomendaciones extends Model
{
    use SoftDeletes;
    protected $table = 'categorias';

    public function usuarios()
    {
        return $this->hasOne('App\Usuarios', 'id', 'usuario');
    }

    public function categorias()
    {
        return $this->hasOne('App\Categorias', 'id', 'categoria');
    }
}
