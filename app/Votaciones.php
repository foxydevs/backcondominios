<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Votaciones extends Model
{
    use SoftDeletes;
    protected $table = 'votaciones';

    public function cotizaciones()
    {
        return $this->hasOne('App\Cotizaciones', 'id', 'cotizacion');
    }

    public function usuarios()
    {
        return $this->hasOne('App\Usuarios', 'id', 'usuario');
    }
}
