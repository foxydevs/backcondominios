<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Alertas extends Model
{
    use softDeletes;
    protected $table = 'alertas';

    public function usuarios()
    {
        return $this->hasOne('App\Usuarios', 'id', 'usuario');
    }
}
