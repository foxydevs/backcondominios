<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Entidades extends Model
{
    use SoftDeletes;
    protected $table = 'entidades';

    public function usuarios()
    {
        return $this->hasOne('App\Usuarios', 'id', 'usuario');
    }

    public function sucursales()
    {
        return $this->hasOne('App\Sucursales', 'id', 'condominio');
    }
}
