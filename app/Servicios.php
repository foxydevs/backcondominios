<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Servicios extends Model
{
    use SoftDeletes;
    protected $table = 'servicios';

    public function sucursales()
    {
        return $this->hasOne('App\Sucursales', 'id', 'condominio');
    }
}
