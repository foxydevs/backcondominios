<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Mensajes extends Model
{
    use SoftDeletes;
    protected $table = 'mensajes';

    public function usuarioEmisor()
    {
        return $this->hasOne('App\Usuarios', 'id', 'usuarioEmisor');
    }

    public function usuarioReceptor()
    {
        return $this->hasOne('App\Usuarios', 'id', 'usuarioReceptor');
    }

    public function usuarioAdministrador()
    {
        return $this->hasOne('App\Usuarios', 'id', 'usuarioAdministrador');
    }
}
