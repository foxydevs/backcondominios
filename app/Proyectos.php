<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Proyectos extends Model
{
    use SoftDeletes;
    protected $table = 'proyectos';

    public function sucursales()
    {
        return $this->hasOne('App\Sucursales', 'id', 'condominio');
    }
}
