<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Visitas extends Model
{
    use SoftDeletes;
    protected $table = 'visitas';

    public function usuarios()
    {
        return $this->hasOne('App\Usuarios', 'id', 'usuario');
    }

    public function entidades()
    {
        return $this->hasOne('App\Entidades', 'id', 'entidad');
    }
}
