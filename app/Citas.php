<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Citas extends Model
{
    use SoftDeletes;
    protected $table = 'citas';

    public function entidades()
    {
        return $this->hasOne('App\Entidades', 'id', 'entidad');
    }

    public function servicios()
    {
        return $this->hasOne('App\Servicios', 'id', 'servicio');
    }
}
